import Cropper from "cropperjs"
import 'cropperjs/dist/cropper.css';

/**
 * Imaginatool main class.
 *
 * @export
 * @class Imaginatool
 */
export default class Imaginatool {
    /**
     *Creates an instance of Imaginatool.
     * @param {string} element id of the imaginatool element
     * @param {string} imageUrl (optionnal) start image url
     * @memberof Imaginatool
     */
    constructor(element, imageUrl) {
        this.element = document.getElementById(element)
        
        this.imageUrl = imageUrl ? imageUrl : ""

        // Cropper js config
        this.config = {
            crop(event) {
                console.log(event.detail.x);
                console.log(event.detail.y);
                console.log(event.detail.width);
                console.log(event.detail.height);
                console.log(event.detail.rotate);
                console.log(event.detail.scaleX);
                console.log(event.detail.scaleY);
            }
        }
        
        // Image input
        this.imageInputClass = "image_input"
        this.createImageInput()
        
        // Launch the tool
        this.imageElement = null
        this.startTool()
    }

    /**
     * Creates an image element and an instance of cropper js working on it.
     *
     * @memberof Imaginatool
     */
    startTool() {
        // Create the image element for cropperjs and input
        this.imageElement = document.createElement("img")
        
        this.imageElement.setAttribute("src", this.imageUrl)
        this.element.appendChild(this.imageElement)

        // Create a new instance of cropperjs
        this.cropper = new Cropper(this.imageElement, this.config)
    }


    /**
     * Destroy image element and cropper instance and relaunch the tool
     *
     * @memberof Imaginatool
     */
    restartTool() {
        this.element.removeChild(this.imageElement)
        this.cropper.destroy()
        this.startTool()
    }


    /**
     * Creates the file input to select image to edit
     *
     * @memberof Imaginatool
     */
    createImageInput() {
        this.imageInput = document.createElement("input")
        this.imageInput.className = this.imageInputClass
        this.imageInput.setAttribute("type", "file")
        
        this.element.appendChild(this.imageInput)

        // Listen to user action on the input
        this.imageInput.addEventListener("input", ()=>{this.fileUploadHandler()})
    }


    /**
     * Changes image url property and restars the tool when new file is selected
     *
     * @memberof Imaginatool
     */
    fileUploadHandler() {
        const fileField = this.imageInput
        const file = fileField.files[0]
        const fileReader = new FileReader()
        
        if((typeof file) === 'object') fileReader.readAsDataURL(file)
        
        fileReader.onload = (event) => {
            // insertElementsOnPage()
            // buildModal(image_source)
            this.imageUrl = event.target.result
            this.restartTool()
        }
    }


}